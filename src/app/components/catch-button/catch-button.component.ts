import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css']
})
export class CatchButtonComponent implements OnInit {

  // Empty string for catching pokemon
  @Input() pokemonId: string = "";


  // Property
  get loading(): boolean {
    return this.catchService.loading;
  }

  constructor(
    private readonly catchService: CatchService
  ) { }

  ngOnInit(): void {
  }

  // Function to catch an pokemon that also uses the pokemonId string
  onCatchClick(): void {
    alert("You have caught a wild: " +  " " + this.pokemonId);
     this.catchService.addCatchedPokemon(this.pokemonId)
     .subscribe({
      next: (response: Trainer) => {
       console.log("Next", response)
      },
      error: (error: HttpErrorResponse) => {
            console.log("ERROR", error.message)
      }
     })
  }


}
 