import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {


   @Input () pokemons: Pokemon[] = [];

   // Keeps track of the user
   private _trainer?: Trainer;
  // Property for trainer
   get trainer(): Trainer | undefined {
     return this._trainer;
   }


  constructor(
    private dataService: DataService
  ) { }

  

  ngOnInit(): void {
    // Sending API call when we render the page
    this.dataService.getPokemonData();
  }

  // Function to show caught pokemons in profile page
  public inCatched(name: string): boolean {
    if(sessionStorage.getItem('pokemon-trainer')?.includes(JSON.stringify(name))) {
      return true;
    }

    return false
  }

  // Function to retrieve pokemons from the API call
  get pokedex(): any[] {
    return this.dataService.getPokedex();
  }

}
