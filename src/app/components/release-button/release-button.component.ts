import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';


@Component({
  selector: 'app-release-button',
  templateUrl: './release-button.component.html',
  styleUrls: ['./release-button.component.css']
})
export class ReleaseButtonComponent implements OnInit {

  @Input () pokemon?: Pokemon;

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  constructor() { }



  ngOnInit(): void {

  }
  
  // Function to release an caught pokemon
  public releasePokemon(name: string): void {
    if(this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter((pokemon: Pokemon) => pokemon.name !== name)
    }
  }

}
