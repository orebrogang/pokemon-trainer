// Module imports
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

// Component imports
import { AppComponent } from './app.component';
import { LoginFormComponent } from './components/login/login-form.component';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { LoginPage } from './pages/login/login.page';
import { ProfilePage } from './pages/profile/profile.page';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CatchButtonComponent } from './components/catch-button/catch-button.component';
import { TrainerPokemonListComponent } from './components/trainer-pokemon-list/trainer-pokemon-list.component';
import { ReleaseButtonComponent } from './components/release-button/release-button.component';

// Decorator. Lets the system know this component exists
@NgModule({
  declarations: [ // Components
    AppComponent,
    ProfilePage,
    LoginPage,
    CataloguePage,
    LoginFormComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    NavbarComponent,
    CatchButtonComponent,
    TrainerPokemonListComponent,
    ReleaseButtonComponent
  ],
  imports: [ // Modules
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
