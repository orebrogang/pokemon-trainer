import { Injectable } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { StorageKeys } from '../../enums/storage-keys.enum';
import { Trainer } from '../../models/trainer.model';
import { StorageUtil } from '../../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  // Keeps track of the user
  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }
  // Displays caught pokemons on the profile page
  public inCatched(name: string): boolean {
    if(this._trainer) {
      return Boolean(this.trainer?.pokemon.find((pokemon: Pokemon) => pokemon.name === name))
    }

    return false
  }

  public releasePokemon(name: string): void {
    if(this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter((pokemon: Pokemon) => pokemon.name !== name)
    }
  }
}
