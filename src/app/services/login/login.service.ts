import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable, of, switchMap } from "rxjs";
import { Trainer } from "src/app/models/trainer.model";
import { environment } from "src/environments/environment";


// Variable for getting the Key and Url from environment file
const { apiUsers, API_KEY } = environment;

@Injectable({
    providedIn: 'root'
})

export class LoginService {

    // Dependency Injection
    constructor(private readonly http: HttpClient) { }
    // Login function that checks if the user already exists
    public login(username: string): Observable<Trainer> {
        return this.checkUsername(username)
        .pipe(
            switchMap((trainer: Trainer | undefined) => {
                // If user is undefined a user will be created with given name
                if (trainer === undefined) {
                    return this.createUser(username);
                }
                return of(trainer);
            })
        )
    }
    // Checks if a user exists
    private checkUsername(username: string): Observable<Trainer | undefined> {
        return this.http.get<Trainer[]>(`${apiUsers}?username=${username}`)
        .pipe(
            map((response: Trainer[]) => response.pop())
        )
    }

    // Create user function
    private createUser(username: string): Observable<Trainer> {
        const user = {
            username,
            pokemon: []
        };
        // Header / API Key
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-api-key": API_KEY
        });
        // POST - Creates users on the server
        return this.http.post<Trainer>(apiUsers, user, {
            headers
        })
    }


}