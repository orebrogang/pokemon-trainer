import {
  HttpClient,
  HttpHeaderResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { DataService } from './data.service';
import { TrainerService } from './trainer/trainer.service';

const { API_KEY, apiUsers } = environment;

@Injectable({
  providedIn: 'root',
})
export class CatchService {
  private _loading: boolean = false
  
  get loading(): boolean {
    return this._loading;
  }
  
  
  constructor(
    private http: HttpClient,
    private readonly dataService: DataService,
    private readonly trainerService: TrainerService
  ) {}


  //Get the pokemon based on name

  public addCatchedPokemon(name: string): Observable<Trainer> {
   
   
    if (!this.trainerService.trainer) {
      throw new Error('addCatchedPokemon: There is no trainer!');
    }

    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined =
      this.dataService.pokemonByName(name);

   

    if (this.trainerService.inCatched(name)) {
      throw new Error(
        'addCatchedPokemon: Pokemon already catched ' + name
      );
    }

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': API_KEY,
    });

    this._loading = true;

    return this.http.patch<Trainer>(
      `${apiUsers}/${trainer.id}`,
      {
        pokemon: [...trainer.pokemon, name],
      },
      {
        headers,
      })
      .pipe(
        tap((updatedTrainer: Trainer) => {
          this.trainerService.trainer = updatedTrainer;
        }),
        finalize(() => {
          this._loading = false;
        })
      )
  }
}
