import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Pokemon } from '../models/pokemon.model';

const {apiPokemons} = environment;

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  private SPRITE_URL =
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';
  private pokedex: any[] = [];

  constructor(
    private http: HttpClient
  ) { }

  //Get pokemons

  public getPokemonData() {
    this.http.get<any>(`${apiPokemons}?limit=20`).subscribe(
      (response: any) => {
        console.log('success');
        this.pokedex.push(this.processData(response.results));
      },
      (error) => console.log('error occured', error)
    );
  }

  // Function to help include an image for each pokemon
  public processData(data: any) {
    let dataTransform: any[] = [];
    let i: number = 0;
    for (let item of data) {
      dataTransform.push({
        data: data[i],
        image_url: this.SPRITE_URL + (i + 1) + '.png',
      });
      ++i;
    }
    return dataTransform;
  }


  // Function to retrieve pokemons from the API call
  public getPokedex() {
    return this.pokedex[0];
  }

  // Getting pokemons by their names
  public pokemonByName(name: string): Pokemon | undefined {
    return this.pokedex.find((pokemon: Pokemon) => pokemon.name === name);
  }

  
}
