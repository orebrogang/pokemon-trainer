import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { CataloguePage } from "./pages/catalogue/catalogue.page";
import { LoginPage } from "./pages/login/login.page";
import { ProfilePage } from "./pages/profile/profile.page";



const routes: Routes = [
    {   // Sets the startup page to be /login
        path: "",
        pathMatch: "full",
        redirectTo: "/login" 
    },
    {   // Links the login path with the right page
        path: "login",
        component: LoginPage
    },
    {   // Links the catalogue path with the right page
        path: "catalogue",
        component: CataloguePage,
        canActivate: [ AuthGuard ]
    },
    {   // Links the profile path with the right page
        path: "profile",
        component: ProfilePage,
        canActivate: [ AuthGuard ]
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ], // Import a module
    exports: [
        RouterModule
    ] // Expose module and it's features
})

export class AppRoutingModule {

}