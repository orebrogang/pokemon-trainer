import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login.page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage {

  constructor(private readonly router: Router) { }

  ngOnInit(): void {
  }

  handleLogin(): void { // Redirects to the catalogue page
    this.router.navigateByUrl("/catalogue");
  }

}
