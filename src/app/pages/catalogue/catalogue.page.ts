import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-catalogue.page',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
export class CataloguePage implements OnInit {

  constructor(
    private readonly dataService: DataService
  ) { }

  // Retreving the fetched data
  ngOnInit(): void {
    this.dataService.getPokedex();
  }

}
