import { Pokemon } from "./pokemon.model";

// Trainer model
export interface Trainer {
    id: number;
    username: string;
    pokemon: Pokemon[];
}